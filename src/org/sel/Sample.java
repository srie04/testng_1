package org.sel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.function.UnaryOperator;

public class Sample {

	public static void main(String[] args) {

		List li = new LinkedList<Object>();
		li.add(10);
		li.add("String");
		li.add('M');
		li.add(1234.123123);
		li.add(10);
		li.add(null);

		System.out.println(li);

		List mi = new ArrayList();
		mi.add(10);
		mi.add("String2");
		mi.add('F');
		mi.add(123.12);

//		Object remove = li.remove(4);
//		System.out.println(li);

		boolean contains = li.contains(null);
		System.out.println(contains);
		
//		
//		li.retainAll(mi);
//		System.out.println(li);
//		
//		
//		li.removeAll(li);
//		System.out.println(li);
//		
//		li.add(50);
//		System.out.println(li);
		
		int indexOf = li.indexOf('M');
		System.out.println(indexOf);
		
		for (int i = 0; i < li.size(); i++) {
			System.out.println(li.get(i));
		}
		
		for (Object object : mi) {
			System.out.println(object);
		}
	}

}
