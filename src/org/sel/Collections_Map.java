package org.sel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Collections_Map {
	
	public static void main(String[] args) {
		
		//key value pair
		
		
		Map<String, Integer> m = new HashMap<String, Integer>();
		
		m.put("Team", 1);
		m.put("Points", 16);
		m.put("won", 2);
		m.put(null, null);
		m.put("java", null);
		
		System.out.println(m);
		
		//to find the size
		
		int size = m.size();
		System.out.println(size);
		
		//to check the particular key is present
		boolean containsKey = m.containsKey(null);
		System.out.println(containsKey);
		
		//to check the particular value is present 
		boolean containsValue = m.containsValue(2);
		System.out.println(containsValue);
		
		//to get values by passing the key
		Integer integer = m.get("Points");
		System.out.println(integer);
		
		//to get all the keys in the map
		Set<String> keySet = m.keySet();
		System.out.println(keySet);
		
		for (String string : keySet) {
			System.out.println(string);
		}
		
		//to get all the values present in a map
		Collection<Integer> values = m.values();
		System.out.println(values);
		
		for (Integer integer2 : values) {
			System.out.println(integer2);
		}
		
		//to iterate the Map
		
		Set<Entry<String,Integer>> entrySet = m.entrySet();
		for (Entry<String, Integer> entry : entrySet) {
			System.out.println(entry);
		}
		
		//to print key and value
		Set<Entry<String,Integer>> entrySet1 = m.entrySet();
		for (Entry<String, Integer> entry : entrySet1) {
			
			
			String key = entry.getKey();
			System.out.println(key);
			Integer value = entry.getValue();
			System.out.println(value);
		}
		
		
		
		
		
		
	}

}
