package org.sel;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Alert_Actions_Navigate {

	public static void main(String[] args) throws InterruptedException, IOException {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\sridhar.elumalai\\Documents\\JavaSeleniumFramework\\drivers\\chromedriver.exe");

		// Open Chrome Browser

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		FileReader reader = new FileReader("C:\\Users\\sridhar.elumalai\\Documents\\Selenium\\file.properties");
		// open a website
		Properties p = new Properties();
		p.load(reader);
	
		
		driver.get(p.getProperty("url"));
		Thread.sleep(2000);
		
		// class obj = new Classnmae(driver)
		// Actions action = new Actions(driver);
		//
		// WebElement okAndCancel = driver.findElement(By.xpath("//a[contains(text(),'OK
		// & Cancel')]"));
		// okAndCancel.click();
		//
		// Thread.sleep(2000);
		//
		// WebElement clickTheBtn =
		// driver.findElement(By.xpath("(//button[contains(text(),'click the
		// button')])[2]"));
		// clickTheBtn.click();

		Alert a = driver.switchTo().alert();

		// a.accept();

		a.dismiss();
		//
		// a.sendKeys();

	}

}
