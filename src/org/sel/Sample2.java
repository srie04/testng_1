package org.sel;

public class Sample2 {

	//Parent Class
	public Sample2() {
	System.out.println("Parent Class non-parameterized constructor");
	}
	
	
	public Sample2(int b) {
		this();
		System.out.println("Parent Class Integer");
	}
	
	
}
